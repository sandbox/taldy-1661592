Drupal.pipeHandlers = Drupal.pipeHandlers || {};

(function ($) {
  /* Sample of pipeHandler
  Drupal.pipeHandlers.test = {
    onStart: function(name, data, options) {
      data.qqq = {bb: 16};
    },
    onSuccess: function(name, result, options) {
    }
  }; */

  Drupal.behaviors.ajaxPipe = {
    attach: function(context) {
      var pipeName = $('body').hasClass('init-pipe-processed') ? 'update' : 'init';

      $('*:eq(0)', context).once('ajax-pipe', function() {
        Drupal.attachPipe(pipeName, {}, context);
      });

      if(pipeName == 'init') {
        $('body').addClass('init-pipe-processed');
      }
    }
  }

  Drupal.attachPipe = function(name, data, context) {
    new Drupal.pipe(name, data, {context: context});
  }

  Drupal.pipe = function(name, data, options) {
    this.name = name;
    this.data = data || {};
    this.options = options || {};

    var defaultOptions = {
      url: Drupal.settings.basePath+'ajax/pipe/'+this.name,
      broadcast: true,
      sendIfEmpty: false
    }

    $.extend(this.options, defaultOptions);
    var pipe = this;

    if(pipe.options.broadcast) {
      $.each(Drupal.pipeHandlers, function () {
        if ($.isFunction(this.onStart)) {
          this.onStart(pipe.name, pipe.data, pipe.options);
        }
      });
    }

    var emptyData = true;
    for(var key in pipe.data) {
      if(key) {
        emptyData = false;
        break;
      }
    }
    if(emptyData && !pipe.options.sendIfEmpty) {
      return false;
    }

    pipe.data = {data: pipe.data};

    // Add HTML Ids and files lists
    Drupal.ajax.prototype.beforeSerialize({}, pipe);
    pipe.commands = Drupal.ajax.prototype.commands;
    pipe.getEffect = Drupal.ajax.prototype.getEffect;
    pipe.effect = 'none';
    pipe.speed = 'none';

    $.ajax({
      url: pipe.options.url,
      data: pipe.data,
      dataType: 'json',
      type: 'POST',
      success: function (response, status) {
        if (typeof response == 'string') {
          response = $.parseJSON(response);
        }
        return pipe.success(response, status);
      }
    });
  }

  Drupal.pipe.prototype.success = function(response, status) {
    var pipe = this;
    $.each(Drupal.pipeHandlers, function () {
      if ($.isFunction(this.onSuccess)) {
        this.onSuccess(pipe.name, response, pipe.options);
      }
    });

    for (var i in response) {
      if (response[i]['command'] && pipe.commands[response[i]['command']]) {
        pipe.commands[response[i]['command']](pipe, response[i], status);
      }
    }
  }

})(jQuery);
